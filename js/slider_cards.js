(function () {
  var rotate, timeline;
  rotate = function () {
    return $('.card:first-child').fadeOut(400, 'linear', function () {
      return $('.card:first-child').appendTo('.slider-cards').hide();
    }).fadeIn(600, 'linear');
  };
  stimeline = setInterval(rotate, 4000);

  $('body').hover(function () {
    return clearInterval(timeline);
  });

  $('.card').click(function () {
    return rotate();
  });

}).call(this);