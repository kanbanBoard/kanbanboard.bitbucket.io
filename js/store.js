import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

const state = {
  users: {
    nextUserId: 7,
    items: [
      {
        id: 1,
        name: "Дмитрий Иванов",
        position: "Разработчик",
        organization: "Code studio",
        location: "Москва",
        email: "d.ivanov@dz.com",
        phoneNumber: "+7 (967) 132-55-12",
        userPhoto: "img/profile_7.jpg",
      },
      {
        id: 2,
        name: "Александр Волошин",
        position: "",
        organization: "",
        location: "",
        email: "",
        phoneNumber: "",
        userPhoto: "img/profile_1.jpg",
      },
      {
        id: 3,
        name: "Анна Сидорова",
        position: "Дизайнер",
        organization: "",
        location: "",
        email: "",
        phoneNumber: "",
        userPhoto: "img/profile_5.jpg",
      },
      {
        id: 4,
        name: "Артем Фадеев",
        position: "",
        organization: "",
        location: "",
        email: "",
        phoneNumber: "",
        userPhoto: "img/profile_4.jpg",
      },
      {
        id: 5,
        name: "Николай Алехин",
        position: "",
        organization: "",
        location: "",
        email: "",
        phoneNumber: "",
        userPhoto: "img/profile_2.jpg",
      },
      {
        id: 6,
        name: "Диана Москвина",
        position: "",
        organization: "",
        location: "",
        email: "",
        phoneNumber: "",
        userPhoto: "img/profile_6.jpg",
      },
    ],
  },
  boards: {
    nextBoardId: 4,
    items: [
      {
        id: 0,
        name: "",
        isPersonal: true,
      },
      {
        id: 1,
        name: "Мобильная разработка",
        isPersonal: false,
      },
      {
        id: 2,
        name: "Дизайн",
        isPersonal: false,
      },
      {
        id: 3,
        name: "Изучить",
        isPersonal: true,
      },
    ],
  },
  boardSelected: {
    id: null,
  },
  lists: {
    nextListId: 13,
    items: [
      {
        id: 1,
        boardId: 0,
        wipLimit: null,
        listName: "Запланировано",
      },
      {
        id: 2,
        boardId: 0,
        wipLimit: 3,
        listName: "В работе",
      },
      {
        id: 3,
        boardId: 0,
        wipLimit: null,
        listName: "Сделано",
      },
      {
        id: 4,
        boardId: 1,
        wipLimit: null,
        listName: "Запланировано",
      },
      {
        id: 5,
        boardId: 1,
        wipLimit: 3,
        listName: "В работе",
      },
      {
        id: 6,
        boardId: 1,
        wipLimit: 3,
        listName: "Сборка",
      },
      {
        id: 7,
        boardId: 1,
        wipLimit: null,
        listName: "Проверка",
      },
      {
        id: 8,
        boardId: 2,
        wipLimit: null,
        listName: "Требования",
      },
      {
        id: 9,
        boardId: 2,
        wipLimit: null,
        listName: "В работе",
      },
      {
        id: 10,
        boardId: 2,
        wipLimit: null,
        listName: "Готово",
      },
      {
        id: 11,
        boardId: 3,
        wipLimit: null,
        listName: "Список",
      },
      {
        id: 12,
        boardId: 3,
        wipLimit: null,
        listName: "Готово",
      },
    ],
  },
  cards: {
    nextCardId: 23,
    items: [
      {
        id: 4,
        boardId: 1,
        listId: 4,
        name: 'Сверстать окно "Карточка товара" ',
        description:
          "Основная информация берется с сервера. Информация о продажах из моков.",
        productName: "Мой бизнес",
        productVersion: "1.5.0",
        userId: 1,
      },
      {
        id: 5,
        boardId: 1,
        listId: 4,
        name: "Сверстать окно 'Cписок продаж' ",
        description: "",
        productName: "Мой бизнес",
        productVersion: "1.5.0",
        userId: 2,
      },
      {
        id: 6,
        boardId: 1,
        listId: 4,
        name: "Перевести экраны с детализацией выручки и информацией о магазине на osync",
        description: "",
        productName: "Мой бизнес",
        productVersion: "1.5.0",
        userId: 1,
      },
      {
        id: 7,
        boardId: 1,
        listId: 5,
        name: "Работа с фильтром в приложении для Osync",
        description: "",
        productName: "Мой бизнес",
        productVersion: "1.5.0",
        userId: 4,
      },
      {
        id: 8,
        boardId: 1,
        listId: 5,
        name: "Исправление ошибок по модулю задач ",
        description: "",
        productName: "Мой бизнес",
        productVersion: "1.5.0",
        userId: 4,
      },
      {
        id: 9,
        boardId: 1,
        listId: 6,
        name: 'Реализовать работу блока "Продажи" на экране "Инфо о магазине" ',
        description: "",
        productName: "Мой бизнес",
        productVersion: "1.5.0",
        userId: 5,
      },
      {
        id: 10,
        boardId: 1,
        listId: 6,
        name: "Добавить таб-бар в мобильное приложение",
        description: "Вкладки продажи, уведомления, настройки. Для уведомлений показывать пустой VC.",
        productName: "Мой бизнес",
        productVersion: "1.5.0",
        userId: 1,
      },
      {
        id: 11,
        boardId: 1,
        listId: 6,
        name: "Встроить UITextView внутрь CustomAlertController ",
        description: "",
        productName: "Мой бизнес",
        productVersion: "1.5.0",
        userId: 2,
      },
      {
        id: 12,
        boardId: 1,
        listId: 7,
        name: 'Сверстать окно "Список продавцов" на моках',
        description: "",
        productName: "Мой бизнес",
        productVersion: "1.5.0",
        userId: 2,
      },
      {
        id: 13,
        boardId: 2,
        listId: 8,
        name: 'Редизайн логотипа',
        description: "Уточнить ТЗ",
        productName: 'Сад "Сказка"',
        productVersion: "",
        userId: 2,
      },
      {
        id: 14,
        boardId: 2,
        listId: 8,
        name: 'Resize изображений',
        description: "",
        productName: 'Сибирь',
        productVersion: "",
        userId: 6,
      },
      {
        id: 15,
        boardId: 2,
        listId: 10,
        name: 'Иллюстрации ко дню города',
        description: "",
        productName: '',
        productVersion: "",
        userId: 6,
      },
      {
        id: 16,
        boardId: 2,
        listId: 10,
        name: 'Прототип страницы личного профиля',
        description: "Добавить ленту событий, информацию о пользователе, закрепить понравившиеся записи",
        productName: '"Сервис доставки"',
        productVersion: "",
        userId: 2,
      },
      {
        id: 17,
        boardId: 2,
        listId: 9,
        name: 'Цветовые темы к панели навигации',
        description: "Использовать цвета из брендбука. Обязательно темное и светлое решение",
        productName: '"Сервис доставки"',
        productVersion: "",
        userId: 2,
      },
      {
        id: 18,
        boardId: 3,
        listId: 12,
        name: "CSS",
        description: "Способы организации стилей; псевдоэлементы; БЭМ",
        productName: "",
        productVersion: "",
        userId: null,
      },
      {
        id: 19,
        boardId: 3,
        listId: 11,
        name: "JavaScript",
        description: "ES6",
        productName: "",
        productVersion: "",
        userId: null,
      },
      {
        id: 20,
        boardId: 3,
        listId: 11,
        name: "Vue.js",
        description: "Изучить цикл жизни компонента, источники данных (data, computed, props)",
        productName: "",
        productVersion: "",
        userId: null,
      },
      {
        id: 21,
        boardId: 3,
        listId: 12,
        name: "html",
        description: "",
        productName: "",
        productVersion: "",
        userId: null,
      },
      {
        id: 22,
        boardId: 3,
        listId: 12,
        name: "HTML",
        description: "",
        productName: "",
        productVersion: "",
        userId: null,
      },
      // USER CARDS CLONE
      {
        id: 1,
        boardId: 0,
        listId: 1,
        name: 'Сверстать окно "Карточка товара" ',
        description:
          "Основная информация берется с сервера. Информация о продажах из моков.",
        productName: "Мой бизнес",
        productVersion: "1.5.0",
        userId: 1,
      },
      {
        id: 2,
        boardId: 0,
        listId: 2,
        name: "Добавить таб-бар в мобильное приложение",
        description: "Вкладки продажи, уведомления, настройки. Для уведомлений показывать пустой VC.",
        productName: "Мой бизнес",
        productVersion: "1.5.0",
        userId: 1,
      },
      {
        id: 3,
        boardId: 0,
        listId: 1,
        name: "Перевести экраны с детализацией выручки и информацией о магазине на osync",
        description: "",
        productName: "Мой бизнес",
        productVersion: "1.5.0",
        userId: 1,
      },

    ],
  },
  formUser: {
    fields: [
      {
        id: 1,
        fieldName: "name",
        label: "Имя",
        type: "text",
      },
      {
        id: 2,
        fieldName: "position",
        label: "Должность",
        type: "text",
      },
      {
        id: 3,
        fieldName: "organization",
        label: "Организация",
        type: "text",
      },
      {
        id: 4,
        fieldName: "location",
        label: "Город",
        type: "text",
      },
      {
        id: 5,
        fieldName: "email",
        label: "email",
        type: "text",
      },
      {
        id: 6,
        fieldName: "phoneNumber",
        label: "Телефон",
        type: "text",
      },

    ],
    user: {
      id: null,
      password: "",
      name: "",
      position: "",
      organization: "",
      location: "",
      email: "",
      phoneNumber: "",
      userId: "",
    },
  },
  formCard: {
    fromBoardId: null,
    fromListId: null,
    card: {
      id: null,
      boardId: null,
      listId: null,
      name: "",
      description: "",
      productName: "",
      productVersion: "",
      userId: null,
    },
  },
  formBoard: {
    board: {
      id: null,
      name: "",
      isPersonal: false,
    },
    newList: {
      listName: "",
      wipLimit: null,
      boardId: null,
    },
  },
};

const actions = {
  showUserSettingsForm(context, id) {
    context.commit("showUserSettingsForm", id);
  },
  hideUserSettingsForm(context) {
    context.commit("hideUserSettingsForm");
  },
  editUserSettings(context, user) {
    context.commit("editUserSettings", user);
  },

  addBoard(context, board) {
    context.commit("addBoard", board);
  },
  editBoard(context, board) {
    context.commit("editBoard", board);
  },
  selectBoard(context, boardId) {
    context.commit("selectBoard", boardId);
  },
  showFormBoard(context) {
    context.commit("showFormBoard");
  },
  hideFormBoard(context) {
    context.commit("hideFormBoard");
  },

  addList(context, list) {
    context.commit("addList", list);
  },

  addCard(context, card) {
    context.commit("addCard", card);
  },
  editCard(context, card) {
    context.commit("editCard", card);
  },
  saveEditCard(context, card) {
    context.commit("saveEditCard", card);
  },
  deleteCard(context, cardId) {
    context.commit("deleteCard", cardId);
  },
  showFormCard(context, card) {
    context.commit("showFormCard", card);
  },
  hideFormCard(context) {
    context.commit("hideFormCard");
  },
};

const mutations = {
  showUserSettingsForm(state, user) {
    const clone = Object.assign({}, state.formUser.user);
    state.formUser.user = Object.assign(clone, user);
  },
  hideUserSettingsForm(state) {
    state.formUser.user = {};
  },
  editUserSettings(state, updateData) {
    let initialData = state.users.items.find(user => user.id === updateData.id);
    initialData = Object.assign(initialData, updateData);
  },

  addBoard(state, board) {
    state.boards.items.push(board);
    state.boards.nextBoardId += 1;
  },
  editBoard(state, board) {
    state.formBoard.board = board;
  },
  selectBoard(state, boardId) {
    state.boardSelected.id = boardId;
  },
  hideFormBoard(state) {
    state.formBoard.board = {};
    state.formBoard.newList = {};
    state.formBoard.board.isPersonal = false;
  },

  addList(state, list) {
    state.lists.items.push(list);
    state.lists.nextListId += 1;
  },

  addCard(state, card) {
    state.cards.items.push(card);
    state.cards.nextCardId += 1;
  },
  deleteCard(state, cardId) {
    const card = state.cards.items.find(item => item.id === cardId);
    const index = state.cards.items.indexOf(card);
    state.cards.items.splice(index, 1);
    state.cards.nextCardId -= 1;
  },
  editCard(state, card) {
    const clone = Object.assign({}, card);
    state.formCard.fromBoardId = card.boardId;
    clone.boardId = card.boardId;
    state.formCard.card = Object.assign(state.formCard.card, clone);
  },
  saveEditCard(state, updateCard) {
    let initialData = state.cards.items.find(card => card.id === updateCard.id);
    initialData = Object.assign(initialData, updateCard);
  },
  showFormCard(state, dataFromList) {
    state.formCard.fromListId = dataFromList.id;
    state.formCard.card.listId = dataFromList.id;
    state.formCard.fromBoardId = dataFromList.boardId;
    state.formCard.card.boardId = dataFromList.boardId;
    state.formCard.card.userId = dataFromList.userId;
  },
  hideFormCard(state) {
    state.formCard.show = false;
    state.formCard.fromListId = null;
    state.formCard.fromBoardId = null;
    state.formCard.card = {};
  },
};

const getters = {
  getUserDataById: initialState => id =>
    initialState.users.items.find(user => user.id === id),

  getFieldById: initialState => id =>
    initialState.formUser.fields.find(field => field.id === id),

  getBoardSelected: () => state.boardSelected.id,

  getBoardsPersonal: () => state.boards.items.filter(board => board.isPersonal === true),

  getBoardsCommand: () => state.boards.items.filter(board => board.isPersonal === false),

  getFormBoard: () => state.formBoard,

  getBoardNextId: () => state.boards.nextBoardId,

  getBoardById: initialState => id =>
    Object.values(initialState.boards.items).find(board => board.id === id),

  getListsFromBoard: initialState => id =>
    Object.values(initialState.lists.items).filter(list => list.boardId === id),

  getListNextId: () => state.lists.nextListId,

  getCardsFromList: initialState => id =>
    Object.values(initialState.cards.items).filter(card => card.listId === id),

  getCardsByUserId: initialState => id =>
    initialState.cards.items.filter(card => card.userId === id),

  getFormCard: () => state.formCard,

  getCardNextId: () => state.cards.nextCardId,
};

export default new Vuex.Store({
  state,
  actions,
  mutations,
  getters,
});
