import Vue from 'vue';
import VueRouter from 'vue-router';

import Store from './store.js';
import App from '../App.vue';
import Profile from '../components/pages/Profile.vue';
import Tasks from '../components/pages/Tasks.vue';
import Dashboards from '../components/pages/Dashboards.vue';
import ViewBoard from '../components/pages/ViewBoard.vue';

Vue.use(VueRouter);

const routes = [
  { path: '/', component: Profile },
  { path: '/tasks', component: Tasks },
  { path: '/dashboards', component: Dashboards },
  { path: '/dashboards/viewboard', component: ViewBoard },
];

const router = new VueRouter({
  routes,
});

new Vue({
  el: '#App',
  router,
  store: Store,
  render: h => h(App),
});
