jsPlumb.bind("ready", function(width) {
    jsPlumb.connect({
        connector: ["Straight"],
        source:"left-top",
        target:"right-bottom",
        anchors:[ "Center", "Center"],
        paintStyle:{strokeWidth: 10, stroke:'#2da8e633'}
    });

    jsPlumb.connect({
        connector: ["Straight"],
        source:"right-top",
        target:"left-bottom",
        anchors:[ "Center", "Center"],
        paintStyle:{strokeWidth: 10, stroke:'#2da8e633'}
    });

    jsPlumb.connect({
        connector: ["Straight"],
        source:"left",
        target:"right",
        anchors:[ "Center", "Center"],
        paintStyle:{strokeWidth: 10, stroke:'#2da8e633'}
    });

    $(window).resize(function(){
        jsPlumb.repaintEverything();
        });
});

// https://developer.mozilla.org/en-US/docs/Web/Web_Components/Using_custom_elements
class PersonGallery extends HTMLElement {

 	constructor() {
	    super();

	    var shadow = this.attachShadow({mode: 'open'});
	    var wrapper = document.createElement('span');
		wrapper.setAttribute('class','wrapper');

		/* url фоток нужно задавать из атрибутов */
		var persons = ["img/profile_1.jpg", "img/profile_2.jpg", "img/profile_3.jpg", "img/profile_4.jpg", "img/profile_5.jpg", "img/profile_6.jpg", "img/profile_7.jpg"];
		var photosCount = persons.length;
		if (persons.length < 4) {
			console.log("Должно быть не меньше 4 человек");
			return;
		}

		/* радиус кольца и дистанцию между фотками нужно задавать из атрибутов */
		var radius = 150;
		var distanse = 70;
		
		var photoSize = (2 * Math.PI * radius - (distanse * (photosCount - 1))) / (photosCount - 1); // примерная формула расчета размера фоток
		var center = radius;
		var size = radius + 2 * photoSize
		var step = 2 * Math.PI  / (photosCount - 1)

		// центральная фотка
	    var photo = this.initPhoto(persons[0], photoSize, center, center);
	    wrapper.appendChild(photo);
		
		// остальные фотки по кругу
		for (var i = 1; i < photosCount; i++) {
			var left = center + radius * Math.cos(i * step);
			var top = center + radius * Math.sin(i * step);
	    	var photo = this.initPhoto(persons[i], photoSize, left, top);
	    	wrapper.appendChild(photo);
		}

		// стили
	    var style = document.createElement('style');
	    style.textContent = '.wrapper {' +
	                           'position: relative;' +
	                           'width: ' + size + 'px;' +
	                           'height: ' + size + 'px;' +
							'}' +
							
							'.photo {' +
								'margin: 0 3%;' + 
								'border-radius:50%;' + 
								'border: 0.8em solid #2da8e633;' +
								'z-index:1;' +
							'}';

	    shadow.appendChild(style);
	    shadow.appendChild(wrapper);
	}

	initPhoto(imgUrl, size, left, top) {
		var photo = document.createElement('img');
	    photo.setAttribute('class','photo');
		photo.src = imgUrl;
		photo.style.width = size + 'px';
		photo.style.height = size + 'px';
	    photo.style.position = "absolute";
		photo.style.left = left + 'px';
		photo.style.top = top + 'px';
		return photo;
	}
}

customElements.define('person-gallery', PersonGallery);